import numpy as np
import control
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go


def logarithmify(x):
    return x if x < 1 else 10 ** (x - 1)


letter_zeta = 'ζ'
letter_omega = 'ω'
letter_sigma = 'σ'
subscript_n = 'ₙ'

simulation_time = 30
pole_real_range = [-5, 3]
pole_imag_range = [0, 10]
slider_margin = 20

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H2(children='ПИД управување'),
    html.Div([
        html.Div([dcc.Graph(id='pole_map')], style={'width': '30%', 'display': 'inline-block'}),
        html.Div([dcc.Graph(id='step_response')], style={'width': '70%', 'display': 'inline-block'}),
    ]),

    html.Div([
        html.Div(id='coefficient_p_label'),
        html.Div([dcc.Slider(
            id='coefficient_p',
            min=0,
            max=4,
            step=0.01,
            value=1,
            marks={i: '{}'.format(logarithmify(i))for i in range(0, 5)},
            updatemode='drag',
        )], style={'margin-bottom': slider_margin,
                   'margin-left': slider_margin,
                   'margin-right': slider_margin}),
        html.Div(id='coefficient_i_label'),
        html.Div([dcc.Slider(
            id='coefficient_i',
            min=0,
            max=4,
            step=0.01,
            value=0,
            marks={i: '{}'.format(logarithmify(i)) for i in range(0, 5)},
            updatemode='drag',
        )], style={'margin-bottom': slider_margin,
                   'margin-left': slider_margin,
                   'margin-right': slider_margin}),
        html.Div(id='coefficient_d_label'),
        html.Div([dcc.Slider(
            id='coefficient_d',
            min=0,
            max=4,
            step=0.01,
            value=0,
            marks={i: '{}'.format(logarithmify(i)) for i in range(0, 5)},
            updatemode='drag',
        )], style={'margin-bottom': slider_margin,
                   'margin-left': slider_margin,
                   'margin-right': slider_margin}),
    ]),
])


@app.callback(
    dash.dependencies.Output('pole_map', 'figure'),
    [
        dash.dependencies.Input('coefficient_p', 'value'),
        dash.dependencies.Input('coefficient_i', 'value'),
        dash.dependencies.Input('coefficient_d', 'value'),
     ])
def update_figure(p, i, d):
    p = logarithmify(p)
    i = logarithmify(i)
    d = logarithmify(d)
    system = get_system(p, i, d)
    x = [pole.real for pole in system.pole()]
    y = [pole.imag for pole in system.pole()]
    pole_style = {'size': 20, 'symbol': 'x'}
    poles_trace = go.Scatter(x=x, y=y, mode='markers', marker=pole_style, name='Полови', showlegend=True)
    traces = [poles_trace]
    return {
        'data': traces,
        'title': 'Местоположба на половите на комплексната рамнина',
        'layout': go.Layout(
            xaxis={'range': [-12, 3], 'title': 'Реална оска'},
            yaxis={'range': [-12, 12], 'title': 'Имагинарна оска'},
        )
    }


@app.callback(
    dash.dependencies.Output('step_response', 'figure'),
    [
        dash.dependencies.Input('coefficient_p', 'value'),
        dash.dependencies.Input('coefficient_i', 'value'),
        dash.dependencies.Input('coefficient_d', 'value'),
    ])
def update_figure(p, i, d):
    p = logarithmify(p)
    i = logarithmify(i)
    d = logarithmify(d)
    system = get_system(p, i, d)
    time, step_response = control.step_response(system, T=np.arange(0, simulation_time, 0.05))
    input_trace = {'x': time, 'y': np.ones_like(step_response), 'name': 'Отскочен влез'}
    step_response_trace = {'x': time, 'y': step_response, 'name': 'Отскочен одѕив'}
    traces = [input_trace, step_response_trace]
    return {
        'data': traces,
        'title': 'Отскочен одѕив',
        'layout': go.Layout(
            xaxis={'range': [0, simulation_time], 'title': 'Време'},
            yaxis={'range': [0, 2], 'title': 'Одѕив'},
        )
    }


def get_system(p, i, d):
    process = control.TransferFunction([1], [1, 1, 1])
    pid = control.TransferFunction([d, p, i], [1, 0])
    system = control.feedback(pid * process)
    return system


@app.callback(
    dash.dependencies.Output('coefficient_p_label', 'children'),
    [dash.dependencies.Input('coefficient_p', 'value')])
def display_value(value):
    return 'Kp = {}'.format(logarithmify(value))


@app.callback(
    dash.dependencies.Output('coefficient_i_label', 'children'),
    [dash.dependencies.Input('coefficient_i', 'value')])
def display_value(value):
    return 'Ki = {}'.format(logarithmify(value))


@app.callback(
    dash.dependencies.Output('coefficient_d_label', 'children'),
    [dash.dependencies.Input('coefficient_d', 'value')])
def display_value(value):
    return 'Kd = {}'.format(logarithmify(value))


if __name__ == '__main__':
    app.run_server()
